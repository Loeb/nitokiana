<?php

use App\Competence;
use App\Models\Education;
use App\Models\Experience;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\App;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $skills = Competence::all();
    $experiences = Experience::all();
    $educations = Education::all();
    // App::setLocale('fr');
    // $locale = App::currentLocale();
    // dd($locale);
    return view('welcome',[
        'skills'=>$skills , 
        'exps'=>$experiences,
        'educations'=>$educations
        ]);
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
